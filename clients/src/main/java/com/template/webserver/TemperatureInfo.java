package com.template.webserver;

public class TemperatureInfo {

    private String temp;
    private String otherParty;

    public TemperatureInfo() {
    }

    public String getTemp(){
        return temp;
    }


    public String getOtherParty(){
        return otherParty;
    }
    public void setTemp(String temp){
        this.temp = temp;
    }

    public void setOtherParty(String otherParty){
        this.otherParty=otherParty;
    }
}
