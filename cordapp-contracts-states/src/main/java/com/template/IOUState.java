package com.template;

import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;

import java.util.Arrays;
import java.util.List;

// *********
// * State *
// *********
//@BelongsToContract(IOUContract.class)
public class IOUState implements  LinearState {
    private  final String temperature;
    private  final AbstractParty to;
    private  final AbstractParty from;
    private final UniqueIdentifier linearId;


    public IOUState(String temperature,
                    AbstractParty to, AbstractParty from, UniqueIdentifier linearId) {
        this.temperature = temperature;
        this.to = to;
        this.from = from;
        this.linearId = linearId;

    }




    public AbstractParty getTo() {
        return to;
    }

    public AbstractParty getFrom() {
        return from;
    }

    public String getTemperature() {
        return temperature;
    }

    @Override
    public List<AbstractParty> getParticipants() {
        return Arrays.asList(to, from);
    }


    @Override
    public UniqueIdentifier getLinearId() {
        return linearId;
    }

}